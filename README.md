# lobstre
A TUI client for [mastodon](https://joinmastodon.org/).

## FAQ
### How do I use lobstre?
It's easy! Just clone this repository and run `stack install lobstre`. Then you can start the application using
the `lobstre` command. (TODO: command-line options, ui, and config files)

### I really like lobstre, but I don't have a mastodon account.
You don't have a mastodon account? Really??? In ${currentYear}??? The best course of action, assuming you'd be
a positive contribution to the fediverse community, [is](https://joinmastodon.org/) 
[to](https://joinmastodon.org/) [join](https://joinmastodon.org/) [mastodon](https://joinmastodon.org/).

### 'lobstre'? I thought it was spelled 'lobster'!
It's a common mistake to misspell 'lobstre' -- so common, in fact, that most of us have been doing it our 
entire lives! Now that you know the right spelling, you may rejoice in your correctness.

## License
This application uses the [GPLv3 License](https://www.gnu.org/licenses/gpl-3.0.en.html). That means it's free
as in free <analogized noun>, and you have to keep it that way if you want to modify/redistribute/etc it.
