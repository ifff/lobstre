module Lobstre.Interactive where

import Data.Maybe

import Lobstre.User

-- | An interactive element of a toot, such as a content warning or tag.
data Element =
  EUser User -- ^ Information describing the user who created/boosted a toot.
  | EMention Handle -- ^ A mention of a user. Interacting with this sends you to their profile.
  | ETag String -- ^ A tag, beginning with #. Interacting with this sends you to a list of toots with that tag.
  | ECw String Bool -- ^ A labelled content warning. Interacting with this toggles the content of the toot.
  deriving (Eq, Show)

-- | A reference (by index) to an element within a toot's element list.
type ElementIdx = Int

-- | A typeclass for things holding elements that can be indexed.
class Interactive a where
  -- | A function that for looking up an element by index, and maybe returning
  -- an element if the index is valid.
  getElt :: a -> ElementIdx -> Maybe Element

-- | The content of a toot, which can either be an @Element@ or just normal text.
-- @Element@s are referred to by an index to their location within the @Toot@'s element list.
data Content = CWord String | CElement ElementIdx
  deriving (Eq, Show)

-- | Something that a mastodon user has posted.
data Toot = Toot
  { op :: Element -- ^ The user who originally created this toot.
  , booster :: Maybe Element -- ^ The user who boosted this toot (may not exist).
  , cw :: Maybe Element -- ^ The toot's content warning, if it has one.
  , miscElements :: [Element] -- ^ A list of other elements (mentions, tags) that occur in the toot.
  , content :: [Content]
  , date :: String
  , liked :: Bool -- ^ Whether you've liked this toot.
  , boosted :: Bool -- ^ Whether you've boosted this toot.
  } deriving (Show)

instance Interactive Toot where
  getElt _ n | n < 0 = Nothing
  getElt toot 0 = Just (op toot)
  getElt toot 1
    | b@(Just _) <- booster toot = b
    | isNothing (booster toot) = getElt toot 2
  getElt toot 2
    | w@(Just _) <- cw toot = w
    | isNothing (cw toot) = getElt toot 3
  getElt toot n
    | n < length (miscElements toot) = Just (miscElements toot !! n)
    | otherwise = Nothing
