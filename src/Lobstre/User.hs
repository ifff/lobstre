module Lobstre.User where

-- | The follow/following relationship between a user and you.
data FollowStatus = None | Following | Followed | Mutual
  deriving (Eq, Show)

-- | A user's handle, e.g. "@Gargron".
data Handle = Handle
  { at :: String
  , inst :: String
  } deriving (Eq, Show)

-- | A datatype that models a mastodon user.
data User = User
  { handle :: Handle
  , name :: String
  , followStatus :: FollowStatus
  } deriving (Eq, Show)
