{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Lobstre.UI (runUI) where

import Control.Monad.IO.Class (liftIO)
import qualified Data.Text as T

import Lens.Micro ((^.))
import Lens.Micro.TH

import qualified Graphics.Vty as V
import Brick
import Brick.Forms
  ( Form
  , newForm
  , formState
  , formFocus
  , setFieldValid
  , renderForm
  , handleFormEvent
  , invalidFields
  , allFieldsValid
  , focusedFormInputAttr
  , invalidFormInputAttr
  , checkboxField
  , radioField
  , editShowableField
  , editTextField
  , editPasswordField
  , (@@=) )
import Brick.Focus
  ( focusGetCurrent
  , focusRingCursor )
import qualified Brick.Widgets.Edit as E
import qualified Brick.Widgets.Border as B
import qualified Brick.Widgets.Center as C

data Name = TootField
  deriving (Eq, Ord, Show)

data TootInfo = TootInfo { _content :: T.Text }
  deriving (Show)
makeLenses ''TootInfo

mkForm :: TootInfo -> Form TootInfo e Name
mkForm = newForm [ label "Toot" @@= editTextField content TootField (Just 3) ]
  where label s w = padBottom (Pad 1) $ (vLimit 1 $ hLimit 15 $ str s <+> fill ' ') <+> w

theMap :: AttrMap
theMap = attrMap V.defAttr
  [ (E.editAttr, V.white `on` V.black)
  , (E.editFocusedAttr, V.black `on` V.yellow)
  , (invalidFormInputAttr, V.white `on` V.red)
  , (focusedFormInputAttr, V.black `on` V.yellow) ]

draw :: Form TootInfo e Name -> [Widget Name]
draw f = [C.vCenter $ C.hCenter form]
  where form = B.border $ padTop (Pad 1) $ hLimit 50 $ renderForm f

app :: (T.Text -> IO ()) -> App (Form TootInfo e Name) e Name
app sendToot =
  App { appDraw = draw
      , appHandleEvent = \s ev ->
          case ev of
            VtyEvent (V.EvResize {}) -> continue s
            VtyEvent (V.EvKey V.KEsc []) -> halt s
            VtyEvent (V.EvKey V.KEnter []) -> do
              liftIO $ sendToot $ (formState s)^.content
              continue s
            _ -> do
              s' <- handleFormEvent ev s
              continue s'
      , appChooseCursor = focusRingCursor formFocus
      , appStartEvent = return
      , appAttrMap = const theMap }

runUI :: (T.Text -> IO ()) -> IO ()
runUI sendToot = do
  let buildVty = do
        v <- V.mkVty =<< V.standardIOConfig
        V.setMode (V.outputIface v) V.Mouse True
        return v

      initialTootInfo = TootInfo ""
      f = mkForm initialTootInfo

  initialVty <- buildVty
  f' <- customMain initialVty buildVty Nothing (app sendToot) f

  putStrLn "The starting form state was:"
  print initialTootInfo

  putStrLn "The final form state was:"
  print $ formState f'
