module Lobstre.State where

import Lobstre.User
import Lobstre.Interactive

data Scene =
  SProfile User Toot
  deriving (Show)

-- there are no lenses for @State@, because the type constraint makes that too complicated ;)
data State =
  State { me :: User -- ^ The person that the client is logged in as.
        , scene :: Scene -- ^ The menu/area being shown (e.g. federated timeline, home, etc.)
        } deriving (Show)


