{-# LANGUAGE RecordWildCards, OverloadedStrings #-}

module Main where

import qualified Data.Text as T
import System.IO
import Web.Hastodon

import Lobstre.User
import Lobstre.State
import Lobstre.UI

main :: IO ()
main = do
  handle <- openFile "../info" ReadMode
  contents <- hGetContents handle
  let loginInfo = lines contents
      token = loginInfo !! 0
      email = loginInfo !! 1
      password = loginInfo !! 2
      acctId = AccountId (loginInfo !! 3)
      inst = loginInfo !! 4
      client = HastodonClient inst token
  -- now construct a tooting function and run the brick UI
  let tooter = \s -> postStatus client (T.unpack s) >> return ()
  runUI tooter  
        

